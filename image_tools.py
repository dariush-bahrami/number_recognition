def image(path):
    import numpy as np
    from skimage import io

    img = io.imread(path)
    img = img.reshape(784, 3)
    img = [sum(pixel)/3 for pixel in img]  # converting to gray scale
    img = np.array(img)
    img /= 255
    img = np.ones(784,) - img
    img = np.array([img])
    return img
