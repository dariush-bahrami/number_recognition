from path_data import model_path, test_image_path

from tensorflow.keras.models import load_model

from image_tools import image

# Loading Model
model = load_model(model_path)


test_image = image(test_image_path)

prediction = model.predict_classes(test_image)
print(prediction)
