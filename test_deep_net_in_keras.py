# -*- coding: utf-8 -*-
from path_data import x_valid_path, y_valid_path, model_path

import numpy as np

from random import randint

from tensorflow.keras.models import load_model

# Loading Validation Data
X_valid = np.load(x_valid_path)
y_valid = np.load(y_valid_path)

# Loading Model
model = load_model(model_path)

# Prediction
image_number = randint(0, 10001)
X = X_valid[image_number, ]

# Following line of code correct this Value Error:
# ValueError: Error when checking input: expected dense_1_input to have
# shape (784,) but got array with shape (1,)

X = X.reshape(1, 784)

y = y_valid[image_number]

y_hat = model.predict_classes(X)

print('Correct y is: ', np.argmax(y))
print('Estimated y: ', y_hat)
