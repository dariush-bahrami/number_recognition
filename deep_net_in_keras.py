# -*- coding: utf-8 -*-
"""
Deep Net in Keras
Coded by dAriush
"""

# Importing Dependencies
import os

import numpy as np

from tensorflow import keras
from tensorflow.keras.datasets import mnist
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Dropout  # new!
from tensorflow.keras.layers import BatchNormalization

from path_data import x_valid_path, y_valid_path, model_path

from tensorboard_launcher.windows_launcher import get_tensorboard_instance

# Load Data
(X_train, y_train), (X_valid, y_valid) = mnist.load_data()

# Preprocess Data
X_train = X_train.reshape(60000, 784).astype('float32')
X_valid = X_valid.reshape(10000, 784).astype('float32')

X_train /= 255
X_valid /= 255

n_classes = 10
y_train = keras.utils.to_categorical(y_train, n_classes)
y_valid = keras.utils.to_categorical(y_valid, n_classes)

# Saving Validation Data
if not os.path.exists(x_valid_path):
    np.save(x_valid_path, X_valid)

if not os.path.exists(y_valid_path):
    np.save(y_valid_path, y_valid)

# Design neural network architecture
model = Sequential()

model.add(Dense(64, activation='relu', input_shape=(784,)))
model.add(BatchNormalization())

model.add(Dense(64, activation='relu'))
model.add(BatchNormalization())

model.add(Dense(64, activation='relu'))
model.add(BatchNormalization())
model.add(Dropout(0.2))

model.add(Dense(10, activation='softmax'))

# Configure model
model.compile(loss='categorical_crossentropy', optimizer='adam',
              metrics=['accuracy'])


# Setting up Tensor Board
tensorboard_callback = get_tensorboard_instance()


# Train
model.fit(X_train, y_train, batch_size=128, epochs=20, verbose=1,
          validation_data=(X_valid, y_valid), callbacks=[tensorboard_callback])

# Save Model
model.save(model_path)
